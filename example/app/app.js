'use strict';

/**
 * @ngdoc overview
 * @name cfForm
 * @description
 * # tallerApp
 *
 * Main module of the application.
 */
angular
  .module('project', [
    'cf',
    'ngResource',
    'ngRoute',
    'angular.panels',
    'treeControl'
  ])

  .config(['$routeProvider', 'panelsProvider', function ($routeProvider, panelsProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/views/intro.html',
        controller : 'IntroCtrl'
      })

      .when('/aside', {
        templateUrl: 'app/views/aside.html',
        controller : 'AsideCtrl'
      })

      // Form routes.

      .when('/form-list', {
        templateUrl: 'app/views/index.html',
        controller : 'IndexCtrl'
      })
      .when('/new-form', {
        templateUrl: 'app/views/new.html',
        controller : 'NewCtrl'
      })
      .when('/edit-form/:id', {
        templateUrl: 'app/views/edit.html',
        controller : 'EditCtrl'
      })
      .when('/show-form/:id', {
        templateUrl: 'app/views/show.html',
        controller : 'ShowCtrl'
      })
      .when('/remove-form/:id', {
        templateUrl: 'app/views/remove.html',
        controller : 'RemoveCtrl'
      })
      .when('/builder', {
        templateUrl: 'app/views/builder.html',
        controller : 'BuilderCtrl'
      })
      .when('/concept', {
        templateUrl: 'app/views/concept.html',
        controller : 'ConceptCtrl'
      })

      // Concept routes.

      .when('/concept-list', {
        templateUrl: 'app/views/concept/index.html',
        controller : 'ConceptIndexCtrl'
      })
      .when('/concept-new', {
        templateUrl: 'app/views/concept/new.html',
        controller : 'ConceptNewCtrl'
      })
      .when('/concept-edit/:id', {
        templateUrl: 'app/views/concept/edit.html',
        controller : 'ConceptEditCtrl'
      })
      .when('/concept-show/:id', {
        templateUrl: 'app/views/concept/show.html',
        controller : 'ConceptShowCtrl'
      })
      .when('/concept-remove/:id', {
        templateUrl: 'app/views/concept/remove.html',
        controller : 'ConceptRemoveCtrl'
      })

      .otherwise({
        redirectTo: '/'
      })

    /**
     * Panels.
     */

    panelsProvider
      .add({
        id                   : "concept-versions",
        position             : "left",
        size                 : "25%",
        templateUrl          : "app/views/concept/concept-versions.html",
        controller           : "ConceptVersionsCtrl",
        openCallbackFunction : "openCallback",
        closeCallbackFunction: "closeCallback"
      })

    panelsProvider
      .add({
        id                   : "forms-versions",
        position             : "left",
        size                 : "25%",
        templateUrl          : "app/views/forms-versions.html",
        controller           : "FormsVersionsCtrl",
        openCallbackFunction : "openCallback",
        closeCallbackFunction: "closeCallback"
      })
  }])

  .run(['$rootScope', function ($rootScope) {
    $rootScope.forms = [];
    $rootScope.concepts = [];
    $rootScope.parentSelect = {};
  }])

  .controller('IntroCtrl', ['$scope', function ($scope) {
    $scope.before = true;
    $scope.after = false;
  }])

  .controller('AsideCtrl', ['$scope', '$rootScope', 'CfBuilder', function ($scope, $rootScope, CfBuilder) {
    $scope.fields = [
      {
        className      : "col-xs-12",
        key            : 'key',
        type           : 'input',
        templateOptions: {
          type       : 'text',
          label      : 'Key model',
          placeholder: 'Key model',
          required   : true,
          description: 'The key to model value.',
        }
      },
      //{
      //  key            : 'roles',
      //  "type"         : "select",
      //  templateOptions: {
      //    label    : 'How do you get around in the city',
      //    options  : [
      //      {
      //        "name": "Car"
      //      },
      //      {
      //        "name": "Helicopter"
      //      },
      //      {
      //        "name": "Sport Utility Vehicle"
      //      },
      //      {
      //        "name" : "Bicycle",
      //        "group": "low emissions"
      //      },
      //      {
      //        "name" : "Skateboard",
      //        "group": "low emissions"
      //      },
      //      {
      //        "name" : "Walk",
      //        "group": "low emissions"
      //      },
      //      {
      //        "name" : "Bus",
      //        "group": "low emissions"
      //      },
      //      {
      //        "name" : "Scooter",
      //        "group": "low emissions"
      //      },
      //      {
      //        "name" : "Train",
      //        "group": "low emissions"
      //      },
      //      {
      //        "name" : "Hot Air Baloon",
      //        "group": "low emissions"
      //      }],
      //    valueProp: 'name'
      //  }
      //},

      //{
      //  key            : 'roles',
      //  "type"         : "select",
      //  templateOptions: {
      //    label    : 'How do you get around in the city',
      //    options  : CfBuilder.test(),
      //    valueProp: 'value'
      //  }
      //},

      //{
      //  type: 'ipAddress',
      //  // other stuff
      //}

      //{
      //  className      : "col-xs-12",
      //  key            : 'key',
      //  type           : 'ckeditor',
      //  templateOptions: {
      //    type       : 'text',
      //    label      : 'Key model',
      //    placeholder: 'Key model',
      //    required   : true,
      //    description: 'The key to model value.',
      //    cfoptions  : {'name': 'victor'}
      //  }
      //},

      // Opción para poner una directiva. IMPORTANTE FUNCIONANDO

      //{
      //  key            : 'exampleDirective',
      //  template       : '<div cfconceptdrag ng-model="model[options.key]"></div>',
      //  templateOptions: {
      //    options: {},
      //    label  : 'Example Directive'
      //  }
      //}

      //{
      //  key            : 'exampleDirective',
      //  template       : '<div cfdate class="col-xs-8 col-sm-8" ng-model="model[options.key]"></div>',
      //  templateOptions: {
      //    options: {},
      //    label  : 'Example Directive'
      //  }
      //}

      //{
      //  key            : 'exampleDirective',
      //  template       : '<div cfchosen multiple="true" class="col-xs-12 col-sm-12" ng-model="advancedSearch.process" val="name" broadcast="testing" on="testing-get"></div>',
      //  templateOptions: {
      //    options: {},
      //    label  : 'Example Directive'
      //  }
      //}
    ];
  }])

/**
 * Form Builder controller.
 */

  .controller('IndexCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
    $scope.fields = [];
  }])

  .controller('NewCtrl', ['$scope', '$http', '$location', '$rootScope', 'panels', function ($scope, $http, $location, $rootScope, panels) {
    $scope.formObject = {
      data_form: []
    };

    // Event to capture a parent from the version control.
    $rootScope.$on('update:parent:form:selected', function (event, meta) {
      if (is.propertyDefined(meta, 'data_form')) {
        $scope.formObject.data_form = angular.copy(meta.data_form);
        $scope.parentDataForm = angular.copy(meta.data_form);
      }
      $scope.showForm = true;
    });

    /**
     * Show left panel with version control.
     */
    $scope.showVersionPanel = function () {
      // Open Versions panel.
      panels.open("forms-versions");
    };

    $scope.openVersionConceptPanel = function () {
      // Open Versions panel.
      panels.open("forms-versions");
      $scope.showForm = false;
    };

    /**
     * Create a form when click on "Create Form" button.
     *
     * @param formObject json. Example:
     * {
        "data_form": {                      // Json. Data from cfformdrag directive for concept build.
          "options": {},
          "rootList": [],
          "model": []
        },
        "name": "First Form",               // Name of form.
        "children": [],                     // Children. This array is always empty and only instance in the tree of the version control.
        "id": 1444672909934,                // ID of form.
        "parent": 1444672863831,            // ID from parent of the form. When 0 it means that the form doesn't have parent.
        "is_new_version": true              // Is this a new version of the form?. When false it means that the form be not shown
                                               in the list of versions because the content inherited of the father was not modified,
                                               it therefore is not a new version.
      }
     */
    $scope.createForm = function (formObject) {
      $scope.msg = '';

      formObject.children = [];

      var sendObject = angular.copy(formObject);

      if (!!formObject.name && formObject.name != '') {

        sendObject.id = _.now();
        sendObject.parent = (is.propertyDefined($rootScope.parentFormSelect, 'id')) ? $rootScope.parentFormSelect.id : 0;

        if (sendObject.parent != 0 && md5(angular.toJson($scope.parentDataForm)) == md5(angular.toJson(formObject.data_form))) {
          sendObject.is_new_version = false;
        } else {
          sendObject.is_new_version = true;
        }

        $rootScope.forms.push(sendObject);

        alert('Created: ' + sendObject.name);
        $location.path('/show-form/' + sendObject.id);

        $scope.msg = '';
        $rootScope.parentFormSelect = {};
      } else {
        $scope.msg = {
          type   : 'warning',
          text   : 'The name field is required',
          classes: 'text-warning'
        }
      }
    };
  }])

  .controller('EditCtrl', ['$routeParams', '$scope', '$http', '$rootScope', 'panels', function ($routeParams, $scope, $http, $rootScope, panels) {
    $scope.showForm = true;

    angular.forEach($rootScope.forms, function (value, key) {
      if ($routeParams.id == value.id) {
        $scope.formObject = value;
        if (is.propertyDefined(value, 'data_form')) {
          $scope.parentDataForm = angular.copy(value.data_form);
        }
      }
    });

    // Event to capture a parent from the version control.
    $rootScope.$on('update:parent:form:selected', function (event, meta) {
      if (is.propertyDefined(meta, 'data_form')) {
        $scope.formObject.data_form = angular.copy(meta.data_form);
        $scope.parentNew = angular.copy(meta);
      }
      $scope.showForm = true;
    });

    $scope.openVersionConceptPanel = function () {
      // Open Versions panel.
      panels.open("forms-versions");
      $scope.showForm = false;
    };

    $scope.editForm = function (formObject) {
      var sendObject = angular.copy(formObject);
      angular.forEach($rootScope.forms, function (value, key) {
        if (value.id == formObject.id) {

          if (sendObject.parent != 0 && is.propertyDefined($scope.parentNew, 'data_form') && md5(angular.toJson($scope.parentNew.data_form)) == md5(angular.toJson(formObject.data_form))) {
            sendObject.is_new_version = false;
          } else {
            sendObject.is_new_version = true;
          }

          sendObject.parent = (is.propertyDefined($scope.parentNew, 'id') && sendObject.parent != $scope.parentNew.id) ? $scope.parentNew.id : sendObject.parent;

          $rootScope.forms[key] = sendObject;
          alert('Change: ' + sendObject.name);
        }
      });
    };
  }])

  .controller('ShowCtrl', ['$routeParams', '$scope', '$http', '$rootScope', function ($routeParams, $scope, $http, $rootScope) {

    angular.forEach($rootScope.forms, function (value, key) {
      if ($routeParams.id == value.id) {
        $scope.formObject = value;
      }
    });
  }])

  .controller('RemoveCtrl', ['$routeParams', '$scope', '$http', '$location', '$rootScope', function ($routeParams, $scope, $http, $location, $rootScope) {

    angular.forEach($rootScope.forms, function (value, key) {
      if ($routeParams.id == value.id) {
        $scope.formObject = value;
      }
    });

    $scope.removeForm = function (formObject) {
      angular.forEach($rootScope.forms, function (value, key) {
        if (value.id == formObject.id) {
          $rootScope.forms.splice(key, 1);
          $location.path('/form-list');
        }
      });
    };
  }])

  .controller('BuilderCtrl', ['$scope', function ($scope) {
    $scope.data_form = [];
  }])

  .controller('ConceptCtrl', ['$scope', function ($scope) {
    $scope.concept = {};
  }])

/**
 * Concept controllers.
 * ---------------------------------------------------------------------------------------
 */

  .controller('ConceptIndexCtrl', ['$scope', function ($scope) {
    $scope.concept = {};
  }])

  .controller('ConceptNewCtrl', ['$scope', '$http', '$timeout', '$location', '$rootScope', 'panels', function ($scope, $http, $timeout, $location, $rootScope, panels) {
    $scope.formObject = {
      data_concept: {} // Data from cfconceptdrag directive for concept build.
    };

    // Event to capture a parent from the version control.
    $rootScope.$on('update:parent:selected', function (event, meta) {
      if (is.propertyDefined(meta, 'data_concept')) {
        $scope.formObject.data_concept = angular.copy(meta.data_concept);
        $scope.parentDataConcept = angular.copy(meta.data_concept);
      }
      $scope.showForm = true;
    });

    /**
     * Show left panel with version control.
     */
    $scope.showVersionPanel = function () {
      // Open Versions panel.
      panels.open("concept-versions");
    };

    $scope.openVersionConceptPanel = function () {
      // Open Versions panel.
      panels.open("concept-versions");
      $scope.showForm = false;
    };

    /**
     * Create a concept when click on "Create Concept" button.
     *
     * @param formObject json. Example:
     * {
        "data_concept": {                   // Json. Data from cfconceptdrag directive for concept build.
          "options": {
            "change": 1444673517354.3542
          },
          "rootList": [],
          "model": []
        },
        "name": "First Concept",            // Name of concept.
        "children": [],                     // Children. This array is always empty and only instance in the tree of the version control.
        "id": 1444672909934,                // ID of concept.
        "parent": 1444672863831,            // ID from parent of the concept. When 0 it means that the concept doesn't have parent.
        "is_new_version": true              // Is this a new version of the concept?. When false it means that the concept be not shown
                                               in the list of versions because the content inherited of the father was not modified,
                                               it therefore is not a new version.
      }
     */
    $scope.createConcept = function (formObject) {
      $scope.msg = '';

      formObject.children = [];

      var sendObject = angular.copy(formObject);

      if (!!formObject.name && formObject.name != '') {
        sendObject.id = _.now();
        sendObject.parent = (is.propertyDefined($scope.parentSelect, 'id')) ? $rootScope.parentSelect.id : 0;

        if (sendObject.parent != 0 && md5(angular.toJson($scope.parentDataConcept)) == md5(angular.toJson(formObject.data_concept))) {
          sendObject.is_new_version = false;
        } else {
          sendObject.is_new_version = true;
        }

        $rootScope.concepts.push(sendObject);

        alert('Created: ' + sendObject.name);
        $location.path('/concept-show/' + sendObject.id);
        $scope.msg = '';
        $rootScope.parentSelect = {};
      } else {
        $scope.msg = {
          type   : 'warning',
          text   : 'The name field is required',
          classes: 'text-warning'
        }
      }
    };

    // Example cfchosen (formly)
    // Example chosen (formly)
    $rootScope.$on('chosen broadcast', function () {
      var model = [
        {
          name: 'Bill'
        },
        {
          name: 'Jane'
        }
      ];

      $rootScope.$broadcast('chosen on', model);
    });
  }])

  .controller('ConceptEditCtrl', ['$routeParams', '$scope', '$timeout', '$http', '$rootScope', 'panels', function ($routeParams, $scope, $timeout, $http, $rootScope, panels) {
    $scope.showForm = true;

    angular.forEach($rootScope.concepts, function (value, key) {
      if ($routeParams.id == value.id) {
        $scope.formObject = value;
        if (is.propertyDefined(value, 'data_concept')) {
          $scope.parentDataConcept = angular.copy(value.data_concept);
        }
      }
    });

    // Event to capture a parent from the version control.
    $rootScope.$on('update:parent:selected', function (event, meta) {
      if (is.propertyDefined(meta, 'data_concept')) {
        $scope.formObject.data_concept = angular.copy(meta.data_concept);
        $scope.parentNew = angular.copy(meta);
      }
      $scope.showForm = true;
    });

    $scope.openVersionConceptPanel = function () {
      // Open Versions panel.
      panels.open("concept-versions");
      $scope.showForm = false;
    };

    /**
     * TODO: VCA: Borrar este comentario en español, es solo para recordar.
     * En el editar concepto pueden funcionar las siguientes cosas:
     * - Al seleccionar un nuevo padre del controlador de versiones todo el árbol de versiones puede
     * ser modificado, es decir, si se agregar un nuevo campo se crea una nueva versión o si se selecciona
     * otro padre diferente y se modifica un campo entonces se cambia de padre y se crea nueva versión en
     * el árbol del controlador de versiones.
     *
     * @param formObject
     */
    $scope.editConcept = function (formObject) {
      var sendObject = angular.copy(formObject);
      angular.forEach($rootScope.concepts, function (value, key) {
        if (value.id == formObject.id) {

          if (sendObject.parent != 0 && md5(angular.toJson($scope.parentNew.data_concept)) == md5(angular.toJson(formObject.data_concept))) {
            sendObject.is_new_version = false;
          } else {
            sendObject.is_new_version = true;
          }

          sendObject.parent = (is.propertyDefined($scope.parentNew, 'id') && sendObject.parent != $scope.parentNew.id) ? $scope.parentNew.id : sendObject.parent;

          $rootScope.concepts[key] = sendObject;
          alert('Change: ' + sendObject.name);
        }
      });
    };
  }])

  .controller('ConceptShowCtrl', ['$routeParams', '$scope', '$http', '$rootScope', function ($routeParams, $scope, $http, $rootScope) {
    angular.forEach($rootScope.concepts, function (value, key) {
      if ($routeParams.id == value.id) {
        $scope.formObject = value;
      }
    })
  }])

  .controller('ConceptRemoveCtrl', ['$routeParams', '$scope', '$http', '$location', '$rootScope', function ($routeParams, $scope, $http, $location, $rootScope) {
    angular.forEach($rootScope.concepts, function (value, key) {
      if ($routeParams.id == value.id) {
        $scope.formObject = value;
      }
    });

    $scope.removeConcept = function (formObject) {
      angular.forEach($rootScope.concepts, function (value, key) {
        if (value.id == formObject.id) {
          $rootScope.concepts.splice(key, 1);
          $location.path('/concept-list');
        }
      });
    };
  }])

  .controller('ConceptVersionsCtrl', ['$scope', '$rootScope', 'panels', function ($scope, $rootScope, panels) {

    // Options of tree (treecontrol directive). https://github.com/wix/angular-tree-control
    $scope.treeOptions = {
      nodeChildren : "children",
      dirSelectable: true,
      injectClasses: {
        ul           : "a1",
        li           : "a2",
        liSelected   : "a7",
        iExpanded    : "a3",
        iCollapsed   : "a4",
        iLeaf        : "a5",
        label        : "a6",
        labelSelected: "a8"
      }
    };

    $scope.treedata = [];

    /**
     * Get list of concepts and check if have children.
     * From the have children add one at fewer in order to show it in the version control.
     *
     * @param callback
     */
    self.loadData = function (callback) {
      var treeInit = [];

      // TODO: VCA: Esto seria obtener desde la api.
      angular.forEach($rootScope.concepts, function (value) {

        var children = self.getOneChildren(value.id);

        if (children) { // Saber si tiene hijos, debe venir así desde la API
          value.children = [];
          value.children.push(children);
          treeInit.push(value);
        } else {
          treeInit.push(value);
        }

      });
      callback(treeInit);
    };

    /**
     * Callback when open panel of versions.
     */
    $scope.openCallback = function () {
      $scope.treedata = [];
      $scope.selected = undefined;
      $scope.selectButton = false;

      self.loadData(function (result) {
        $scope.treedata = _.where(result, {parent: 0});
      });
    };

    /**
     * Callback when close panel of versions.
     */
    $scope.closeCallback = function () {
      $scope.treedata = [];
      $scope.selected = undefined;
      if (!$scope.selectButton) {
        $rootScope.parentSelect = {};
      }
      $rootScope.$emit('update:parent:selected', $rootScope.parentSelect);
    };

    self.getOneChildren = function (id) {
      // TODO: VCA: Esto seria obtener los hijos pero preguntandole a la API.
      return _.findWhere(_.where($rootScope.concepts, {is_new_version: true}), {parent: id});
    };

    /**
     * Get all children of parent.
     *
     * @param id
     * @returns {*|Array}
     */
    self.getChildren = function (id) {
      // TODO: VCA: Esto seria obtener los hijos pero preguntandole a la API.
      return _.where(_.where($rootScope.concepts, {is_new_version: true}), {parent: id});
    };

    /**
     * Callback when click on node of version control tree.
     *
     * @param node
     * @param selected
     */
    $scope.showSelected = function (node, selected) {
      $rootScope.parentSelect = node;
      $scope.selectNode = node;

      var $panelDom = jQuery('#cf-concept-versions-template').parent();
      if (selected) {
        $panelDom.css('width', '100%');
        $scope.model = node;
      } else {
        $panelDom.css('width', '25%');
        $scope.model = [];
        $rootScope.parentSelect = {};
      }

      //$node.children = self.getChildren($node.id);
    };

    /**
     * Callback when expand a node of version control tree.
     *
     * @param $node
     * @param expanded
     * @param $parentNode
     */
    $scope.expandedSelected = function ($node, expanded, $parentNode) {
      $node.children = angular.copy(self.getChildren($node.id));
    };

    /**
     * Click on button "selectThisParent()".
     */
    $scope.selectThisParent = function () {
      $scope.selectButton = true;
      panels.close();
    };
  }])

  .controller('FormsVersionsCtrl', ['$scope', '$rootScope', 'panels', function ($scope, $rootScope, panels) {
    // Options of tree (treecontrol directive). https://github.com/wix/angular-tree-control
    $scope.treeOptions = {
      nodeChildren : "children",
      dirSelectable: true,
      injectClasses: {
        ul           : "a1",
        li           : "a2",
        liSelected   : "a7",
        iExpanded    : "a3",
        iCollapsed   : "a4",
        iLeaf        : "a5",
        label        : "a6",
        labelSelected: "a8"
      }
    };

    $scope.treedata = [];

    /**
     * Get list of forms and check if have children.
     * From the have children add one at fewer in order to show it in the version control.
     *
     * @param callback
     */
    self.loadDataForms = function (callback) {
      var treeInit = [];

      // TODO: VCA: Esto seria obtener desde la api.
      angular.forEach($rootScope.forms, function (value) {

        var children = self.getOneChildrenForm(value.id);

        if (children) { // Saber si tiene hijos, debe venir así desde la API
          value.children = [];
          value.children.push(children);
          treeInit.push(value);
        } else {
          treeInit.push(value);
        }

      });
      callback(treeInit);
    };

    /**
     * Callback when open panel of versions.
     */
    $scope.openCallback = function () {
      $scope.treedata = [];
      $scope.selected = undefined;
      $scope.selectButton = false;

      self.loadDataForms(function (result) {
        $scope.treedata = _.where(result, {parent: 0});
      });
    };

    /**
     * Callback when close panel of versions.
     */
    $scope.closeCallback = function () {
      $scope.treedata = [];
      $scope.selected = undefined;
      if (!$scope.selectButton) {
        $rootScope.parentFormSelect = {};
      }
      $rootScope.$emit('update:parent:form:selected', $rootScope.parentFormSelect);
    };

    self.getOneChildrenForm = function (id) {
      // TODO: VCA: Esto seria obtener los hijos pero preguntandole a la API.
      return _.findWhere(_.where($rootScope.forms, {is_new_version: true}), {parent: id});
    };

    /**
     * Get all children of parent.
     *
     * @param id
     * @returns {*|Array}
     */
    self.getChildrenForm = function (id) {
      // TODO: VCA: Esto seria obtener los hijos pero preguntandole a la API.
      return _.where(_.where($rootScope.forms, {is_new_version: true}), {parent: id});
    };

    /**
     * Callback when click on node of version control tree.
     *
     * @param node
     * @param selected
     */
    $scope.showSelected = function (node, selected) {
      $rootScope.parentFormSelect = node;
      $scope.selectNode = node;

      var $panelDom = jQuery('#cf-form-versions-template').parent();
      if (selected) {
        $panelDom.css('width', '100%');
        $scope.model = node;
      } else {
        $panelDom.css('width', '25%');
        $scope.model = [];
        $rootScope.parentFormSelect = {};
      }

      //$node.children = self.getChildren($node.id);
    };

    /**
     * Callback when expand a node of version control tree.
     *
     * @param $node
     * @param expanded
     * @param $parentNode
     */
    $scope.expandedSelected = function ($node, expanded, $parentNode) {
      $node.children = angular.copy(self.getChildrenForm($node.id));
    };

    /**
     * Click on button "selectThisParent()".
     */
    $scope.selectThisParent = function () {
      $scope.selectButton = true;
      panels.close();
    };
  }])

  .factory('Person', [function () {
    return {
      getAll: function () {
        return [
          {
            name    : 'John',
            lastname: 'Doe'
          },
          {
            name    : 'Jane',
            lastname: 'Doe'
          },
          {
            name    : 'Joe',
            lastname: 'Botts'
          },
          {
            name    : 'Joe',
            lastname: 'Botts'
          },
          {
            name    : 'Eddie',
            lastname: 'Punchclock'
          },
          {
            name    : 'Vinnie',
            lastname: 'Boombotz'
          },
          {
            name    : 'Jean',
            lastname: 'Dupont'
          }
        ];
      }
    }
  }])
/**
 * Element: CkEditor.
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', 'formlyConfig', function ($rootScope, formlyConfig) {
    var element = {
      name          : 'ckeditor',
      //extends       : 'input',
      wrapper       : ['bootstrapLabel', 'bootstrapHasError'],
      defaultOptions: {
        templateOptions: {
          label: 'Set the name of the field label'
        }
      },
      template      : '<div>' +
      '<textarea ' +
      'ckeditor="editorOptions" ' +
      'ng-model="model[options.key]"' +
      'class="autosize-transition form-control"' +
      '></textarea></div>',
      link          : function (scope, el, attrs) {
      },
      controller    : ['$scope', function ($scope) {

        $scope.defaultToolbarFullCkeditor = [
          {
            name : 'basicstyles',
            items: ['Bold', 'Italic', 'Strike', 'Underline']
          },
          {name: 'paragraph', items: ['BulletedList', 'NumberedList', 'Blockquote']},
          {name: 'editing', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
          {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
          {name: 'tools', items: ['SpellChecker', 'Maximize']},
          '/',
          {
            name : 'styles',
            items: ['Format', 'FontSize', 'TextColor', 'BGColor', 'PasteText', 'PasteFromWord', 'RemoveFormat']
          },
          {name: 'insert', items: ['Image', 'Table', 'SpecialChar']},
          {name: 'forms', items: ['Outdent', 'Indent']},
          {name: 'clipboard', items: ['Undo', 'Redo']},
          {name: 'document', items: ['PageBreak', 'Source']}
        ];

        $scope.editorOptions = {
          language    : is.propertyDefined($scope.to.cfoptions, 'language') ? $scope.to.cfoptions.language : 'es',
          height      : is.propertyDefined($scope.to.cfoptions, 'height') ? $scope.to.cfoptions.height + 'px' : '100px',
          toolbar_full: $scope.defaultToolbarFullCkeditor,
          enterMode   : 'CKEDITOR.ENTER_BR'
        };

      }],
      apiCheck      : {}
    };

    formlyConfig.setType(element);
    $rootScope.$emit('cf:form:extra', element);
  }]);
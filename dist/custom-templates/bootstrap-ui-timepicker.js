/**
 * Element: AngularJS Bootstrap UI Datepicker.
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', 'formlyConfig', function ($rootScope, formlyConfig) {
    var element = {
      name          : 'bootstrap-ui-timepicker',
      //extends       : 'input',
      wrapper       : ['bootstrapLabel', 'bootstrapHasError'],
      template      : '<timepicker ' +
      'ng-model="model[options.key]" ' +
      'hour-step="to.cfoptions.hourStep" ' +
      'minute-step="to.cfoptions.minuteStep" ' +
      'show-meridian="to.cfoptions.showMeridian">' +
        //'meridians="to.cfoptions.meridians">' +
        //'readonly-input="to.cfoptions.readonlyInput">' +
        //'mousewheel="to.cfoptions.mouseWheel">' +
      '</timepicker>',
      defaultOptions: {
        templateOptions: {
          label    : '',
          cfoptions: {
            hourStep    : 1,
            minuteStep  : 1,
            //showMeridian: true,
            //mouseWheel   : true,
            //readonlyInput: true,
            //meridians : ['AM', 'PM']
          },
          list     : []
        }
      },
      link          : function (scope, el, attrs) {
      },
      controller    : ['$scope', function ($scope) {

      }],
      apiCheck      : {}
    };

    formlyConfig.setType(element);
    $rootScope.$emit('cf:form:extra', element);
  }]);
/**
 * Module Directives specific to app.
 */
'use strict';

angular.module("cfDirectives")

/**
 * Directive to chosen with extra options.
 * Example with "fnc" option:
 *
 * <div cfchosen multiple="true" class="col-xs-12 col-sm-12" ng-model="vehicle" val="name" fnc="getDataChosenVehicles"></div>
 *
 * Example with events, options "broadcast" and "on":
 *
 * <div cfchosen multiple="true" class="col-xs-12 col-sm-12" ng-model="vehicle" val="name" broadcast="broadcast event from cfchosen" on="on event from cfchosen"></div>
 *
 * The directive accepts the following attributes:
 *
 * - 'ng-model':    Scope model.
 * - 'val':         Value by filter and show data.
 * - 'fnc':         Function to execute in external controller.
 * - 'multiple':    Boolean.
 * - 'broadcast':   Name of the event to send by $broadcast in the case of that not passed the parameter [fnc].
 * - 'on':          Name of the event to get by $on in the case of that not passed the parameter [fnc].
 *
 * Example of fnc:
 * In Controller:
 *
 * $scope.getDataChosenVehicles = function (scope, callback) {
 *     Vehicle.all.customGET("", {
 *       code  : 'list',
 *       limit : scope.limit,
 *       offset: scope.offset,
 *       //order_by: $scope.params.sortBy,
 *       search: scope.search
 *     }).then(function (result) { // Success.
 *         $rootScope.$emit('db:error', result.meta);
 *         flashMessageLaunch(result.meta.msg);
 *         callback(result.data);                   // IMPORTANT this line callback, this is required.
 *       },
 *       function (error) { // Error from server.
 *         flashMessageLaunch({type: 'error', text: error.statusText});
 *       })
 *   }
 */
  .directive('cfchosen', ['$document', function ($document) {
    var strTemplate = "";
    strTemplate += "<div>";
    strTemplate += "  <label>{{ label }}<\/label>";
    strTemplate += "  <div class=\"cf-chosen\">";
    strTemplate += "    <div class=\"\">";
    strTemplate += "      <div class=\"well well-sm cf-element-selected\" ng-click=\"init()\"";
    strTemplate += "           style=\"min-height : 33px; margin-bottom: 0px;background-color: #FFF;cursor: pointer;padding: 5px;\">";
    strTemplate += "";
    strTemplate += "        <span ng-show=\"!multiple\">{{ model[val] }}<\/span>";
    strTemplate += "      <span ng-show=\"multiple\" class=\"cf-elements-span label label-sm label-primary\" ng-repeat=\"item in model\"";
    strTemplate += "            style=\"margin-left: 2px;\">";
    strTemplate += "        {{ item[val] }}";
    strTemplate += "        <i class=\"cf-remove-multiple-item glyphicon glyphicon-remove\" ng-click=\"removeChosenMultipleElement($index)\"";
    strTemplate += "           title=\"Remove\" style=\"margin-left: 5px;\"><\/i>";
    strTemplate += "      <\/span>";
    strTemplate += "";
    strTemplate += "        <i class=\"cf-chosen-icon glyphicon pull-right\" style=\"margin-top: 3px;\"><\/i>";
    strTemplate += "      <\/div>";
    strTemplate += "";
    strTemplate += "      <div class=\"cf-form-select\" style=\"position: absolute;top: 58px;z-index: 100;width: 97%;\">";
    strTemplate += "        <div class=\"input-group\">";
    strTemplate += "          <input class=\"cf-chosen-search form-control input-mask-product\" type=\"text\" ng-model=\"search\"";
    strTemplate += "                 style=\"border: 1px solid #428BCA;\">";
    strTemplate += "        <span class=\"input-group-addon\"";
    strTemplate += "              style=\"color: #fff;background-color: #428BCA;border-bottom-color: #428BCA;border: 1px solid #428BCA;\">";
    strTemplate += "            <i class=\"glyphicon glyphicon-search\"><\/i>";
    strTemplate += "        <\/span>";
    strTemplate += "        <\/div>";
    strTemplate += "";
    strTemplate += "        <div>";
    strTemplate += "          <ul class=\"ui-menu ui-widget ui-widget-content\" role=\"menu\"";
    strTemplate += "              style=\"width: 100%;background-color: #fff;max-height: 209px;overflow-x: auto;border: 1px solid #428BCA;padding: 2px;\">";
    strTemplate += "            <li class=\"ui-menu-item cf-element\" role=\"menuitem\" ng-repeat=\"item in items\"";
    strTemplate += "                ng-click=\"thisSelect(item); thisSelectLink($index)\">{{ item[val] }}";
    strTemplate += "            <\/li>";
    strTemplate += "          <\/ul>";
    strTemplate += "          <button ng-show=\"showButtonScroll\" class=\"btn btn-primary btn-block btn-minier no-border\"";
    strTemplate += "                  ng-click=\"infiniteScroll()\">";
    strTemplate += "            <i class=\"glyphicon glyphicon-plus\"><\/i>";
    strTemplate += "          <\/button>";
    strTemplate += "        <\/div>";
    strTemplate += "      <\/div>";
    strTemplate += "    <\/div>";
    strTemplate += "  <\/div>";
    strTemplate += "  <p class=\"help-block\" ng-if=\"description && description != 'undefined'\">{{ description }}<\/p>";
    strTemplate += "<\/div>";

    return {
      restrict  : 'A',
      replace   : true,
      require   : '?ngModel',
      //templateUrl: '../dist/common/directives/views/cfchosen.html',
      template  : strTemplate,
      scope     : {
        model    : '=ngModel',
        //items: '=',
        fnc      : '=?',
        val      : '@',
        multiple : '=',
        broadcast: '@?',
        on       : '@?',

        label      : '@?',
        description: '@?'
      },
      link      : function (scope, element, attrs, timeout) {
        var $elementSelected = element.find('.cf-element-selected');
        var $searchInput = element.find('.cf-chosen-search');
        var $formSelect = element.find('.cf-form-select');
        var $iconUpDown = element.find('.cf-chosen-icon');

        function cfchosenFormShow() {
          $iconUpDown.removeClass('glyphicon-chevron-down');
          $iconUpDown.addClass('glyphicon-chevron-up');
          $formSelect.show();
          $elementSelected.removeClass('closed');
          $elementSelected.addClass('opened');
          cfchosenUpdateAllDom();
        }

        function cfchosenFormHide() {
          $iconUpDown.removeClass('glyphicon-chevron-up');
          $iconUpDown.addClass('glyphicon-chevron-down');
          $formSelect.hide();
          $elementSelected.removeClass('opened');
          $elementSelected.addClass('closed');
        }

        function cfchosenUpdateAllDom() {
          element.find('.cf-element').each(function () {
            jQuery(this).removeClass('ui-state-disabled');
          })

          if (scope.multiple) {
            angular.forEach(scope.items, function (value, key) {
              angular.forEach(scope.model, function (valueModel) {
                if (_.isEqual(valueModel, value)) {
                  element.find('.cf-element').eq(key).addClass('ui-state-disabled');
                }
              })
            });
          } else {
            angular.forEach(scope.items, function (value, key) {
              if (_.isEqual(scope.model, value)) {
                element.find('.cf-element').eq(key).addClass('ui-state-disabled');
              }
            });
          }
        }

        cfchosenFormHide();

        element.bind('click', function (e) {
          element.find('.cf-remove-multiple-item').bind('click', function (et) {
            cfchosenUpdateAllDom();
            et.stopPropagation();
          });
        });

        $searchInput.bind('click', function (e) {
          e.stopPropagation();
        });

        // When click to select a element.
        $elementSelected.bind('click', function (e) {
          if ($elementSelected.hasClass('closed')) {
            cfchosenFormShow();
          } else {
            cfchosenFormHide();
          }

          e.stopPropagation();
        });

        scope.thisSelectLink = function (index) {
          cfchosenUpdateAllDom();
          cfchosenFormHide();
        }

        scope.$watch('items', function () {
          cfchosenUpdateAllDom();
        }, true);

        scope.$watch('search', function () {
          cfchosenUpdateAllDom();
        }, true);

        $document.on('click', function () {
          cfchosenFormHide();
        });

      },
      controller: ['$scope', '$parse', '$timeout', '$rootScope', function ($scope, $parse, $timeout, $rootScope) {
        var executeFunction = $parse($scope.fnc);

        $scope.items = [];
        $scope.showButtonScroll = true;

        $scope.limit = 10;
        $scope.offset = 0;
        $scope.search = '';

        if ($scope.multiple) {
          $scope.model = [];
        }

        $scope.init = function () {
          if ($scope.items.length == 0) {
            cfchosenExecute();
          }
        }

        $scope.thisSelect = function (item) {
          if ($scope.multiple) {
            if (!_.findWhere($scope.model, _.omit(item, '$$hashKey'))) {
              $scope.model.push(item);
            } else {
              alert('The value was selected.');
            }
          } else {
            $scope.model = item;
          }
        }

        $scope.infiniteScroll = function () {
          cfchosenExecute()
        }

        $scope.removeChosenMultipleElement = function (index) {
          $scope.model.splice(index, 1);
        }

        $scope.$watch('search', function () {
          $scope.offset = 0;
          $scope.items = [];
          cfchosenExecute();
        }, true);

        function cfchosenExecute() {
          if (angular.isDefined($scope.fnc)) {
            executeFunction($scope, function (items) {
              $scope.items = _.union($scope.items, items);

              if (items.length < $scope.limit) {
                $scope.showButtonScroll = false;
              } else {
                $scope.showButtonScroll = true;
              }

              $scope.offset = $scope.offset + $scope.limit;
            });
          } else if (!angular.isDefined($scope.fnc)) {
            $scope.broadcast = angular.isDefined($scope.broadcast) ? $scope.broadcast : '';
            if ($scope.broadcast) {
              $rootScope.$broadcast($scope.broadcast, $scope.model);
            }
          }
        };

        $scope.on = angular.isDefined($scope.on) ? $scope.on : '';
        $rootScope.$on($scope.on, function (event, items) {
          $scope.items = _.union($scope.items, items);

          if (items.length < $scope.limit) {
            $scope.showButtonScroll = false;
          } else {
            $scope.showButtonScroll = true;
          }

          $scope.offset = $scope.offset + $scope.limit;
        });
      }]
    }
  }]);
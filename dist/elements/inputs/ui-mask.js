/**
 * Element: AngularJS Chosen from chosen plugin.
 * Directive
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', function ($rootScope) {
    var element = {
      parent     : ['all', 'input'],
      title      : 'UI Mask',
      description: 'Mask on an input field so the user can only type pre-determined pattern.'
    };

    element.model = {};

    /**
     * Function build, build and return json with field formly structure.
     *
     * @param modelJson
     * @param callback
     * @returns {{key: *, type: string, defaultValue: (*|fieldOptionsApiShape.defaultValue|string|field.defaultValue|g.defaultValue|Variable.defaultValue), templateOptions: {type: string, label: *, placeholder: (*|string|element.modelFields.templateOptions.placeholder|MainCtrl.fields.templateOptions.placeholder|Task._placeholder.placeholder|$scope.fields.placeholder), required: *, description: (*|string|element.modelFields.templateOptions.description|description|type.__apiCheckData.description|n.x.n.description)}}}
     */
    element.fbuild = function (modelJson, callback) {
      var field = {
        className      : (modelJson.className) ? modelJson.className : 'col-md-12',
        key            : modelJson.key,
        type           : 'ui-mask',
        templateOptions: {
          label      : modelJson.label,
          required   : modelJson.required,
          cfoptions  : modelJson,
          description: modelJson.description,
        }
      };

      if (modelJson.addonLeftClass || modelJson.addonRightClass) {
        if (!!modelJson.addonLeftClass) {
          field.templateOptions.addonLeft = {
            class: modelJson.addonLeftClass
          }
        }

        if (!!modelJson.addonRightClass) {
          field.templateOptions.addonRight = {
            class: modelJson.addonRightClass
          }
        }

        setTimeout(function () {
          callback(field);
        }, 1000);
      }
      else {
        callback(field);
      }
    };

    /**
     * Model Fields.
     *
     * @type {{className: string, fieldGroup: *[]}[]}
     */

    element.modelFields = [
      {
        "className" : "row",
        "fieldGroup": [
          {
            className      : "col-xs-6",
            key            : 'key',
            type           : 'input',
            templateOptions: {
              type       : 'text',
              label      : 'Key model',
              placeholder: 'Key model',
              required   : true,
              description: 'The key to model value.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'label',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Display name',
              placeholder: 'Display name',
              required   : true,
              description: 'The name of the field label.'
            }
          },

          {
            className      : "col-xs-12",
            key            : 'uiMask',
            type           : 'doc-ui-mask',
            templateOptions: {
              label      : 'Options ui mask',
              //placeholder: '(999) 999-9999 ext. ?9?9?9',
              //required   : false,
              description: ''
            }
          },

          {
            className      : "col-xs-6",
            key            : 'required',
            type           : 'input',
            templateOptions: {
              type       : 'checkbox',
              label      : 'Required?',
              placeholder: '',
              required   : false,
              description: 'Field is required?.'
            }
          },
          {
            className      : "col-xs-12",
            key            : 'description',
            type           : 'textarea',
            templateOptions: {
              type       : 'textarea',
              label      : 'Description',
              placeholder: 'Description',
              required   : false,
              description: 'Description of the field.',
              rows       : 4
            }
          }
        ]
      }
    ];

    $rootScope.$emit('cf:form:elements', element);
  }]);
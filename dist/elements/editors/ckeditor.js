/**
 * Element: Bootstrap CKEditor.
 * Directive
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', function ($rootScope) {
    var element = {
      parent     : ['all', 'richtext'],
      title      : 'CK Editor',
      description: 'CkEditor Directive (external)'
    };

    element.model = {};

    /**
     * Function build, build and return json with field formly structure.
     *
     * @param modelJson
     * @param callback
     * @returns {{key: *, type: string, defaultValue: (*|fieldOptionsApiShape.defaultValue|string|field.defaultValue|g.defaultValue|Variable.defaultValue), templateOptions: {type: string, label: *, placeholder: (*|string|element.modelFields.templateOptions.placeholder|MainCtrl.fields.templateOptions.placeholder|Task._placeholder.placeholder|$scope.fields.placeholder), required: *, description: (*|string|element.modelFields.templateOptions.description|description|type.__apiCheckData.description|n.x.n.description)}}}
     */
    element.fbuild = function (modelJson, callback) {
      var field = {
        className      : (modelJson.className) ? modelJson.className : 'col-md-12',
        key            : modelJson.key,
        type           : 'ckeditor',
        templateOptions: {
          label      : modelJson.label,
          required   : modelJson.required,
          cfoptions  : modelJson,
          description: modelJson.description,
          options    : {}
        }
      };

      if (modelJson.addonLeftClass || modelJson.addonRightClass) {
        if (!!modelJson.addonLeftClass) {
          field.templateOptions.addonLeft = {
            class: modelJson.addonLeftClass
          }
        }

        if (!!modelJson.addonRightClass) {
          field.templateOptions.addonRight = {
            class: modelJson.addonRightClass
          }
        }

        setTimeout(function () {
          callback(field);
        }, 1000);
      }
      else {
        callback(field);
      }
    };

    /**
     * Model Fields.
     *
     * @type {{className: string, fieldGroup: *[]}[]}
     */

    element.modelFields = [
      {
        "className" : "row",
        "fieldGroup": [
          {
            className      : "col-xs-6",
            key            : 'key',
            type           : 'input',
            templateOptions: {
              type       : 'text',
              label      : 'Key model',
              placeholder: 'Key model',
              required   : true,
              description: 'The key to model value.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'label',
            type           : 'input',
            templateOptions: {
              type       : 'string',
              label      : 'Display name',
              placeholder: 'Display name',
              required   : true,
              description: 'The name of the field label.'
            }
          },
          //{
          //  className      : "col-xs-12",
          //  key            : 'defaultToolbarFullCkeditor',
          //  type           : 'textarea',
          //  defaultValue   : "[{name : 'basicstyles',items: ['Bold', 'Italic', 'Strike', 'Underline']},{name: 'paragraph', items: ['BulletedList', 'NumberedList', 'Blockquote']},{name: 'editing', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},{name: 'links', items: ['Link', 'Unlink', 'Anchor']},{name: 'tools', items: ['SpellChecker', 'Maximize']},'/',{name : 'styles',items: ['Format', 'FontSize', 'TextColor', 'BGColor', 'PasteText', 'PasteFromWord', 'RemoveFormat']},{name: 'insert', items: ['Image', 'Table', 'SpecialChar']},{name: 'forms', items: ['Outdent', 'Indent']},{name: 'clipboard', items: ['Undo', 'Redo']},{name: 'document', items: ['PageBreak', 'Source']}]",
          //  templateOptions: {
          //    type       : 'textarea',
          //    label      : 'Default Toolbar Full CKeditor',
          //    placeholder: '',
          //    required   : false,
          //    description: 'Guided by the option [toolbar_full] in http://nightly.ckeditor.com',
          //    rows       : 6
          //  }
          //},
          {
            className      : "col-xs-6",
            key            : 'language',
            "type"         : "select",
            defaultValue   : 'es',
            templateOptions: {
              label      : 'Select Language',
              options    : [
                {
                  "name" : "Spanish",
                  "value": "es"
                },
                {
                  "name" : "France",
                  "value": "fr"
                },
                {
                  "name" : "Italy",
                  "value": "it"
                },
                {
                  "name" : "English",
                  "value": "en"
                }],
              valueProp  : 'value',
              description: ''
            }
          },
          {
            className      : "col-xs-6",
            key            : 'height',
            type           : 'input',
            templateOptions: {
              type       : 'text',
              label      : 'Height of Editor',
              placeholder: 'Height of Editor',
              required   : false,
              description: 'By default es 100.'
            }
          },
          {
            className      : "col-xs-6",
            key            : 'required',
            type           : 'input',
            templateOptions: {
              type       : 'checkbox',
              label      : 'Required?',
              placeholder: '',
              required   : false,
              description: 'Field is required?.'
            }
          },
          {
            className      : "col-xs-12",
            key            : 'description',
            type           : 'textarea',
            templateOptions: {
              type       : 'textarea',
              label      : 'Description',
              placeholder: 'Description',
              required   : false,
              description: 'Description of the field.',
              rows       : 4
            }
          }
        ]
      }
    ];

    $rootScope.$emit('cf:form:elements', element);
  }]);


/**
 * Module Directives specific to app.
 */
angular.module("cf")

/**
 * Directive to show form:
 * Example:
 * <div cfform-show ng-model="model" fields="fields"></div>
 */
  .directive('cfformShow', [function () {
    return {
      restrict: 'E',
      replace : true,
      require : '?ngModel',

      scope: {
        model : '=ngModel',
        fields: '='
      },

      template: '<div><formly-form model="model" fields="fields"></formly-form></div>'
    }
  }]);